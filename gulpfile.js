const {watch, src, dest, series, parallel} = require('gulp');
const sass = require('gulp-sass');
sass.compiler = require('node-sass');
const postcss = require('gulp-postcss');
const uncss = require('postcss-uncss');
const del = require('del');
const browserSync = require('browser-sync').create();
const autoprefixer = require('gulp-autoprefixer');
const imagemin = require('gulp-imagemin');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');

function copyHtml() {
    return src('./src/index.html')
        .pipe(dest('./dist'))
        .pipe(browserSync.reload({ stream: true }));
}

function copyJs() {
    return src('./src/js/**.js')
        .pipe(concat('scripts.min.js'))
        .pipe(uglify())
        .pipe(dest('./dist/js'))
        .pipe(browserSync.reload({ stream: true }));
}

function buildScss() {
    return src('./src/scss/*.scss')
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(postcss([
            uncss({html: ['./dist/index.html']})]))
        .pipe(concat('styles.min.css'))
        .pipe(autoprefixer({
            overrideBrowserslist: ["last 5 versions"],
            cascade: true
        }))
        .pipe(dest('./dist/css'))
        .pipe(browserSync.stream());
}

function copyAssets() {    // -
    return src('./src/assets/**')
        .pipe(dest('./dist/assets'))
}
function minImg() {
    return src('src/assets/img/**')
        .pipe(imagemin())
        .pipe(dest('dist/assets/img/'))
}

function emptyDist() {
        return del('./dist/**');
}

function browsyn() {
    browserSync.init({
        server: './dist'
    });

    watch(['./src/index.html'], copyHtml);
    watch(['./src/scss/*.scss'], buildScss);
    watch(['./src/js/*.js'], copyJs);
}

exports.html = copyHtml;
exports.scss = buildScss;
exports.assets = copyAssets;
exports.clear = emptyDist;
exports.js = copyJs;
exports.sync = browsyn;
exports.minimg = minImg;

const build = series(
    emptyDist,
    parallel(
        series(copyHtml , buildScss),
        copyAssets,
        minImg,
        copyJs
    )
);

exports.build = build;

exports.dev = series(build, browsyn);
